# -*- coding: utf-8 -*-
"""
Created on Fri May 11 22:59:18 2018

@author: kyleq
"""


def timeToMinutes(time):
    """get total minutes from a time in a day"""

    hourStr, minuteStr = time.split(':')
    hour = int(hourStr)
    minute = int(minuteStr)
    return hour * 60 + minute

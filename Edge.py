# -*- coding: utf-8 -*-
"""
Created on Fri May 11 19:15:58 2018

@author: kyleq
"""


class Edge:
    """Edges on map"""

    def __init__(self, firstPointMarker, secondPointMarker, weight):
        self.firstPointMarker = firstPointMarker
        self.secondPointMarker = secondPointMarker
        self.weight = weight

// Ant.h
//
// Created on: NOV 12, 2014
// Author: wshao
//
// Ant.h
//
// Created on: Dec 1, 2014
// Author: wshao
//

#ifndef ANT_H_
#define ANT_H_

#include <time.h>
#include<iostream>
#include<string>


#include"Point.h"

struct weights
{
	double distance;
	double pheromone;
	double deltaPheromone;
	double probability;
};

class Ant
{
public:
	Ant(){
		m_dLength = 0;
		m_time = 0;
		alpha = 1.0;
		beta = 2.0;
	}
	~Ant(){}

	void m_moveToNextPoint(const Point start, Point& NPT, vector<Point>& nodes, map<string, map<string, weights>>& edges);
	void m_reIni(vector<string>& searchSpace);
	void m_iniTabu(vector<Point>& nodes);
	void m_iniTabuBySerachSpace(vector<string>& serachSpace);

	int m_dLength;
	double m_time;
	double rho;
	vector<string> m_path;

private:
	double rnd(int low, double uper)
	{
		double p = (rand() / (double)RAND_MAX)*((uper)-(low)) + (low); // generate a double from low to uper
		return (p);
	};

	void m_iniProb(const Point start, map<string, double>& pro, vector<Point>& nodes, map<string, map<string, weights>>& edges);

	int rnd(int uper)
	{
		return (rand() % uper); // generate an int from 0 to uper
	};
	map<string, bool> m_tabu; // true means this points has been added to path
	map<string, double> Probability;
	double alpha;
	double beta;
};

#endif
#include "Ant.h"

void Ant::m_moveToNextPoint(const Point start, Point& NPT, vector<Point>& nodes, map<string, map<string, weights>>& edges)
{
	m_tabu[start.m_Maker] = true;
	m_iniProb(start, Probability, nodes,edges);

	double mRate = rnd(0, 1);
	double mSelect = 0;
	map<string, double>::iterator it = Probability.begin();

	for (; it != Probability.end(); it++) {

		if (m_tabu.find(it->first)->second == false)
			mSelect += it->second;
		if (mSelect >= mRate)
		{
			NPT.m_Maker = it->first;
			break;
		}
	}
}

void Ant::m_reIni(vector<string>& searchSpace) 
{
	map<string, bool>::iterator it = m_tabu.begin();

	for (; it != m_tabu.end(); it++)
	{
		it->second = true;
	}

	vector<string>::iterator itS = searchSpace.begin();
	for (; itS != searchSpace.end(); itS++)
	{
		m_tabu[*itS] = false;
	}
	
	m_path.clear();
	m_dLength = 0;
	m_time = 0;

	// m_dLength = m_path.size();
}

void Ant::m_iniTabu(vector<Point>& nodes)
{
	vector<Point>::iterator itN = nodes.begin();

	for (; itN != nodes.end(); itN++)
	{
		m_tabu.insert(pair<string, bool>(itN->m_Maker, true));
	}
}


void Ant::m_iniTabuBySerachSpace(vector<string>& serachSpace)
{
	vector<string>::iterator itS = serachSpace.begin();
	for (; itS != serachSpace.end(); itS++)
	{
		m_tabu.find(*itS)->second = false;
	}
}




void Ant::m_iniProb(const Point start, map<string, double>& pro, vector<Point>& nodes,map<string,map<string,weights>>& edges)
{
	//calculate total probability 

	double tmpSumProbability = 0;;

	map<string, map<string, weights>>::iterator itEdge = edges.find(start.m_Maker);

	vector<Point>::iterator itNode = nodes.begin(); 

	for (; itNode != nodes.end(); itNode++)
	{
		if (m_tabu.find(itNode->m_Maker)->second == false)
			tmpSumProbability += pow(itEdge->second.find(itNode->m_Maker)->second.pheromone, alpha) * pow(itNode->probobality, beta);
	}


	double P = -1;
	string name;

	itEdge = edges.find(start.m_Maker);

	for (itNode = nodes.begin(); itNode != nodes.end(); itNode++)
	{
		name = itNode->m_Maker;
		if (m_tabu[name] == false)
		{
			P = pow(itEdge->second.find(itNode->m_Maker)->second.pheromone,alpha) * pow(itNode->probobality, beta) / tmpSumProbability;
		}
		else
		{
			P = 0;
		}
		pro[name] = P;
	}
}
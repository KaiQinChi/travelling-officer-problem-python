//
// CarPoint.cpp
//
// Created on: NOV 25, 2014
// Author: wshao
//
//
// Point.h
//
// Created on: Nov 25, 2014
// Author: wshao
//
#ifndef POINT_H_
#define POINT_H_

#include <map>
#include "Area.h"


struct record{
	string maker;
	double vioTime;
	double depTime;
	bool isCheck;
	bool isSense;
};

class Point
{
public:
	Point()
	{
		x = -1;
		y = -1;
		violation = false;
		m_index = -1;
	}
	Point(double x, double y, bool violation, int index)
	{
		this->x = x;
		this->y = y;
		this->violation = violation;
		m_index = index;
	}
	~Point()
	{}


	bool operator = (const Point &pt);
	bool operator == (const Point &ps);

	string m_Maker;
	double x;
	double y;
	int m_index;
	bool violation;
	double probobality;
	double calP(double time,double updateTime);

	vector<record> records;
private:

};

#endif

#include"Point.h"

bool Point::operator = (const Point &ps)
{
	this->x = ps.x;
	this->y = ps.y;
	this->m_Maker = ps.m_Maker;
	this->violation = ps.violation;

	this->records = ps.records;
	this->probobality = ps.probobality;
	return true;
}

bool Point::operator == (const Point &ps)
{
	if (this->m_Maker == ps.m_Maker)
		return true;
	else
		return false;

}

double Point::calP(double time,double curTime)
{
	vector<record>::iterator itRecord = records.begin();

	for (; itRecord != records.end(); itRecord++)
	{
		if (curTime < itRecord->vioTime || itRecord->isCheck == true || curTime> itRecord->depTime)
			continue;
		else
			return Area::pLeave[int(time - itRecord->vioTime)];
	}
	return 0.0;
}
# -*- coding: utf-8 -*-
"""
Created on Fri May 11 19:15:58 2018

@author: kyleq
"""


class Point:
    """Point on map for each parking slot """

    def __init__(self, id_=-1, longitude=-1, latitude=-1, marker=None):
        self.longitude = longitude
        self.latitude = latitude
        self.id_ = id_
        self.vioRecords = []
        self.marker = marker
        self.violated = False
        self.probability = -1.0

    def calculateProbability(self, arriveTime, curTime, leaveProList):
        """Calculate leveling probability for a arriving point"""

        for record in self.vioRecords:
            if curTime < record.vioTime or record.isCheck is True or curTime > record.depTime:
                continue
            else:
                index = arriveTime - record.vioTime
                self.probability = leaveProList[int(index)]
                return self.probability
        self.probability = 0.0
        return self.probability

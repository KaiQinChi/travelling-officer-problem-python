# -*- coding: utf-8 -*-
"""
Created on Fri May 18 10:58:20 2018

@author: Kai Qin
"""


class MyLog:
    testTag = 'test: '
    level = 1

    def debugTest(mes):
        print(MyLog.testTag + str(mes))

    def infoTest(mes):
        print(MyLog.testTag + str(mes))

    def debugTag(tag, mes):
        print(tag + ': ' + str(mes))

    def infoTag(tag, mes):
        print(tag + ': ' + str(mes))

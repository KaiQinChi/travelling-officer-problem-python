# -*- coding: utf-8 -*-
"""
Created on Fri May 11 19:16:11 2018

@author: kyleq
"""
import random
from Log import MyLog


class Ant:
    """ """

    def __init__(self):
        self.m_dLength = 0
        self.antUsedTime = 0
        self.rho = 0.0
        self.alpha = 1.0
        self.beta = 2.0
        self.m_path = []
        self.m_tabu = {}  # true means this points has been added to path
        self.probability = {}

    def randomFloat(low, upper):
        p = random.uniform(low, upper)
        return p

    def randomInt(upper):
        return random.randint(0, upper)

    def initialTabu(self, nodes=[]):
        for node in nodes:
            self.m_tabu.update({node.marker: True})

    def initialTabuBySearchSpace(self, searchSpace=[]):
        for space in searchSpace:
            self.m_tabu[space] = False

    def initialReDo(self, searchSpace):
        for key in self.m_tabu.keys():
            self.m_tabu[key] = True

        for space in searchSpace:
            self.m_tabu[space] = False

        self.m_path = []
        self.antUsedTime = 0
        self.m_dLength = len(self.m_path)

    def moveToNextPoint(self, startPoint, NPTPoint, nodes, edges):
        self.m_tabu[startPoint.marker] = True
        self.iniProbability(startPoint, self.probability, nodes, edges)

        mRate = Ant.randomFloat(0, 1)
        mSelect = 0

        for key, value in self.probability.items():
            if self.m_tabu[key] is False:
                mSelect += value
            if mSelect >= mRate:
                NPTPoint.marker = key
                break
        if NPTPoint.marker is None:
            NPTPoint.marker = list(self.probability.keys())[0]

    def iniProbability(self, startPoint, pros, nodes, edges):
        # calculate total probability
        tmpSumProbability = 0
        itEdges = [edge for edge in edges if edge.firstPointMarker == startPoint.marker]
        MyLog.infoTag('iniProbability startPoint', startPoint.marker)
        for node in nodes:
            if self.m_tabu[node.marker] is False:
                MyLog.infoTag('iniProbability node.marker', node.marker)
                e = [edge for edge in itEdges if edge.secondPointMarker == node.marker]
                tmpSumProbability += pow(e[0].weight.pheromone, self.alpha) * pow(node.probability, self.beta)

        for node in nodes:
            name = node.marker
            if self.m_tabu[name] is False and tmpSumProbability != 0.0:
                e = [edge for edge in itEdges if edge.secondPointMarker == node.marker]
                P = pow(e[0].weight.pheromone, self.alpha) * pow(node.probability, self.beta) / tmpSumProbability
            else:
                P = 0
            pros.update({name: P})

# -*- coding: utf-8 -*-
"""
Created on Fri May 11 19:10:52 2018

@author: kyleq
"""
from numpy import genfromtxt
from Point import Point
from Officer import Officer
from CarParkMap import CarParkMap

startTime = 480  # Officer begin to collect money from this time
endTime = 1100  # Officer end collecting fines at this time
updateTime = 5  # the violation information update frequency
speed = 70  # officers' avg speed

locationFileName = 'data/chinatown_location.csv'
distanceFileName = 'data/chinatown_distance.csv'
violatedRecordFileName = 'data/Chinatown9_record.csv'
leavingFileName = 'data/chinatown_pro.csv'

# read distribution of leaving probability in the current area
leaveProList = genfromtxt(leavingFileName, delimiter=',')
leaveProList = [1.0 - x for x in leaveProList]

carParkMap = CarParkMap()
carParkMap.initialMap(locationFileName, distanceFileName, violatedRecordFileName)
startPoint = Point(0, 0, 0, 'start')
officer = Officer(carParkMap, leaveProList, 1, speed)

algorithm = 1
if algorithm == 1:
    outputFileName = 'greed_result.csv'
if algorithm == 2:
    outputFileName = 'nature_result.csv'
if algorithm == 3:
    outputFileName = 'ACO_result.csv'

officer.findPath(startPoint, startTime, algorithm, endTime, updateTime)
officer.showPath()
carParkMap.writePathToFile(officer.m_path, outputFileName)

# -*- coding: utf-8 -*-
"""
Created on Fri May 11 19:15:58 2018

@author: kyleq
"""
from Point import Point
from Edge import Edge
from Utils import timeToMinutes
from Ant import Ant
import pandas as pd
import csv


class Weight:
    """Information for edges and nodes on map"""

    def __init__(self, distance, probability, pheromone=0, deltaPheromone=0):
        self.distance = distance
        self.pheromone = pheromone
        self.deltaPheromone = deltaPheromone
        self.probability = probability


class ViolatedRecord:
    """Violated record history for one point"""

    def __init__(self, marker, vioTime, depTime, isCheck=False, isSense=False):
        self.marker = marker
        self.vioTime = vioTime
        self.depTime = depTime
        self.isCheck = isCheck
        self.isSense = isSense


class CarParkMap:
    """Car parking map"""

    def __init__(self, m_rho=0.3):
        self.m_rho = m_rho
        self.nodesSize = 0
        self.nodes = []
        self.edges = []
        self.signals = []
        self.ants = []

    def initialMap(self, locationFileName, distanceFileName, violatedRecordFileName):
        """Initial map with edges and nodes"""

        parkingSlotsData = pd.read_csv(locationFileName)
        violatedRecordData = pd.read_csv(violatedRecordFileName)
        distanceData = pd.read_csv(distanceFileName)

        # create each node of two points and read distance for edge
        for index, row in parkingSlotsData.iterrows():
            point = Point(index, row['longitude'], row['latitude'], row['marker'])
            # read their violated records for each node
            for index1, row1 in violatedRecordData.iterrows():
                if point.marker == row1['marker']:
                    vioTime = timeToMinutes(row1['vioTime'])
                    depTime = timeToMinutes(row1['depTime'])

                    point.vioRecords.append(ViolatedRecord(row1['marker'], vioTime, depTime))
                    self.signals.append(ViolatedRecord(row1['marker'], vioTime, depTime))
            self.nodes.append(point)

        # create each edge of two points and read distance for edge
        for node1 in self.nodes:
            firstMarker = node1.marker
            for node2 in self.nodes:
                secondMarker = node2.marker
                # get distance for an edge if it exist in data set
                entry = distanceData[
                    (distanceData.firstPoint == firstMarker) & (distanceData.secondPoint == secondMarker)]
                if entry.empty:
                    weight = Weight(-1.0, -1.0)
                else:
                    # get distance value at first row
                    dis = entry['distance'].iloc[0]
                    weight = Weight(dis, -1.0)
                self.edges.append(Edge(firstMarker, secondMarker, weight))
        return self.edges, self.nodes

    def getDistance(self, startPMarker, toPMarker):
        """Get distance between two points"""

        e = self.findEdgeByNode(startPMarker, toPMarker)
        if e is None:
            return 0.0
        else:
            return e.weight.distance

    def findEdgeByNode(self, startPMarker, toPMarker):
        e = [edge for edge in self.edges if edge.firstPointMarker == startPMarker and edge.secondPointMarker ==
             toPMarker]

        if not e:
            return None
        else:
            return e[0]

    def initialAnt(self, n):
        for i in range(n):
            ant = Ant()
            ant.initialTabu(self.nodes)
            self.ants.append(ant)

    def initialAntBySearchSpace(self, searchSpace=[]):
        for ant in self.ants:
            ant.initialTabuBySearchSpace(searchSpace)

    def getBestACOPath(self, best, bestTime):
        time = 1441
        benefit = -1
        temp = -1

        for index, ant in enumerate(self.ants):
            ant.m_dLength = len(ant.m_path)
            time = ant.antUsedTime
            if benefit < ant.m_dLength:
                temp = index
                benefit = ant.m_dLength
                continue
            if time > (ant.antUsedTime + 0.000001) and benefit == ant.m_dLength:
                temp = index
                time = ant.antUsedTime

        antTemp = self.ants[temp]
        if len(best) < antTemp.m_dLength:
            best.clear()
            best = antTemp.m_path

        if len(best) == antTemp.m_dLength and bestTime > time:
            best.clear()
            best = antTemp.m_path
        return best

    def initialPheromoneValues(self):
        for edge in self.edges:
            edge.weight.pheromone = 1.0
            edge.weight.deltaPheromone = 0.0

    def updatePheromone(self):
        # calculate deltaPheromone in total search space
        for ant in self.ants:
            length = len(ant.m_path)
            for index in range(length - 1):
                e = self.findEdgeByNode(ant.m_path[index], ant.m_path[index + 1])
                e.weight.deltaPheromone += ant.m_dLength / len(self.nodes)

                if len(self.nodes) == ant.m_dLength:
                    e.weight.deltaPheromone += (1440 * 1440) / (ant.antUsedTime * ant.antUsedTime)
                e.weight.pheromone = e.weight.pheromone * self.m_rho + e.weight.deltaPheromone

        #        for ant in self.ants:
        #            for index in range(length - 1):
        #                e = self.findEdgeByNode(ant.m_path[index], ant.m_path[index + 1])
        #                e.pheromone = e.pheromone * self.m_rho + e.deltaPheromone

        # initialize deltaPheromone value to 0
        for ant in self.ants:
            length = len(ant.m_path)
            for index in range(length - 1):
                e = self.findEdgeByNode(ant.m_path[index], ant.m_path[index + 1])
                e.weight.deltaPheromone = 0.0

    def writePathToFile(self, path, fileName):
        results = [['Marker', 'Latitude', 'Longitude']]
        for point in path:
            for node in self.nodes:
                if node.marker == point.marker:
                    row = [node.marker, node.latitude, node.longitude]
                    results.append(row)
        with open(fileName, "w") as output:
            writer = csv.writer(output, lineterminator='\n')
            writer.writerows(results)

# -*- coding: utf-8 -*-
"""
Created on Fri May 11 19:15:19 2018

@author: kyleq
"""
from Point import Point
import copy
from Log import MyLog


class Officer:
    """Officer to collect fines"""

    maxIterCount = 3
    nAnts = 5

    def __init__(self, carParkMap, leaveProList, id_=0, walkingSpeed=50):
        self.id_ = id_
        self.walkingSpeed = walkingSpeed
        self.benefit = 0.0
        self.saveTime = 0.0
        self.startTime = 0
        self.startPoint = None
        self.endTime = 0
        self.updateTime = 0
        self.m_path = []
        self.m_queue = []
        self.m_searchSpace = []
        self.carParkMap = carParkMap
        self.leaveProList = leaveProList

    def findPath(self, startP, startTime, algorithm, endTime, updateTime):
        self.startTime = startTime
        self.startPoint = copy.deepcopy(startP)
        self.endTime = endTime
        self.updateTime = updateTime

        if algorithm == 1:
            self.greedyFindPath()
        if algorithm == 2:
            self.natureFindPath()
        if algorithm == 3:
            self.ACOFindPath()

    def showPath(self):
        markers = ''
        print('benefit:' + str(self.benefit))
        print('save time:' + str(self.benefit))
        print('path size:' + str(len(self.m_path)))

        for point in self.m_path:
            markers = markers + point.marker + '-->'
        print(markers)

    def greedyFindPath(self):
        nextPoint = Point()
        consumedTime = self.startTime
        self.m_path.clear()
        self.m_path.append(self.startPoint)
        currentPoint = copy.deepcopy(self.startPoint)

        while consumedTime < self.endTime:
            print("greedyFindPath loop... currentPoint...." + str(currentPoint.marker) +
                  " consumedTime...." + str(consumedTime))
            self.moveNextPoint(currentPoint, nextPoint, consumedTime)
            if nextPoint.marker == 'Null':
                consumedTime = consumedTime + 1.0
                self.saveTime = self.saveTime + 1.0
            else:
                self.m_path.append(copy.deepcopy(nextPoint))
                travelTime = self.getTravelTime(currentPoint, nextPoint)
                consumedTime = consumedTime + travelTime
                self.benefit = self.benefit + self.getBenefits(nextPoint, consumedTime)
                self.updateNode(nextPoint, consumedTime)
                currentPoint = copy.deepcopy(nextPoint)

    def moveNextPoint(self, currentPoint, nextPoint, currentTime):
        maxP, minKey = self.getLeavingPossibilityForNodes(currentTime, currentPoint)
        # print("maxP ...." + str(maxP) + " minKey ...." + str(minKey))

        # maxP shall not be start point which is 0.00001
        if maxP <= 0.00001:
            nextPoint.marker = 'Null'
        else:
            nextPoint.marker = minKey

    def getLeavingPossibilityForNodes(self, currentTime, currentPoint):
        maxP = 0.00001
        minKey = 'start'

        for node in self.carParkMap.nodes:
            if node.marker == 'start':
                node.probability = 0.0
                continue
            else:
                travelTime = self.getTravelTime(currentPoint, node)
                arriveTime = currentTime + travelTime
                node.calculateProbability(arriveTime, currentTime, self.leaveProList)
            if maxP < node.probability:
                maxP = node.probability
                minKey = node.marker
        return maxP, minKey

    def calProbability(self, currentTime, currentPoint):
        for node in self.carParkMap.nodes:
            if node.marker == 'start':
                node.probability = 0.0
            else:
                travelTime = self.getTravelTime(currentPoint, node)
                arriveTime = currentTime + travelTime
                node.calculateProbability(arriveTime, currentTime, self.leaveProList)

    def getTravelTime(self, startP, toP):
        dis = self.carParkMap.getDistance(startP.marker, toP.marker)
        return dis / self.walkingSpeed

    def getBenefits(self, point, time):
        for node in self.carParkMap.nodes:
            if point.marker == node.marker:
                for record in node.vioRecords:
                    if time < record.vioTime or record.isCheck is True or time > record.depTime:
                        continue
                    else:
                        return 1.0
        return 0.0

    def updateNode(self, point, time):
        for node in self.carParkMap.nodes:
            if point.marker == node.marker:
                for record in node.vioRecords:
                    if record.vioTime < time < record.depTime:
                        record.isCheck = True
                        break

    def natureFindPath(self):
        nextPoint = Point()
        consumedTime = self.startTime
        self.m_path.clear()
        self.m_path.append(self.startPoint)
        currentPoint = copy.deepcopy(self.startPoint)
        self.updateQueue(consumedTime)

        while consumedTime < self.endTime:
            if not self.m_queue:
                consumedTime = consumedTime + 1.0
                self.updateQueue(consumedTime)
                self.saveTime = self.saveTime + 1.0
            else:
                nextPoint.marker = self.m_queue[0]
                self.m_queue.pop(0)
                self.m_path.append(copy.deepcopy(nextPoint))

                travelTime = self.getTravelTime(currentPoint, nextPoint)
                consumedTime = consumedTime + travelTime
                self.benefit = self.benefit + self.getBenefits(nextPoint, consumedTime)
                self.updateNode(nextPoint, consumedTime)
                self.updateQueue(consumedTime)
                currentPoint = copy.deepcopy(nextPoint)

    def updateQueue(self, consumedTime):
        for signal in self.carParkMap.signals:
            if signal.vioTime < consumedTime < signal.depTime and signal.isSense is False:
                self.m_queue.append(signal.marker)
                signal.isSense = True

    def ACOFindPath(self):
        nextPoint = Point()
        time = self.startTime
        self.m_path.clear()
        self.m_path.append(self.startPoint)
        currentPoint = copy.deepcopy(self.startPoint)

        self.carParkMap.initialAnt(Officer.nAnts)
        while time < self.endTime:
            self.updateSearchSpace(time)
            MyLog.infoTag('search space: ', self.m_searchSpace)
            MyLog.infoTest('Whole Path time ' + str(time))
            if not self.m_searchSpace:
                time = time + 1.0
                self.saveTime = self.saveTime + 1.0
                continue
            self.carParkMap.initialAntBySearchSpace(self.m_searchSpace)
            self.walkToNextNode(currentPoint, nextPoint, time)

            if 'NULL' == nextPoint.marker:
                time = time + 1.0
                self.saveTime = self.saveTime + 1.0
                continue

            self.m_path.append(copy.deepcopy(nextPoint))
            for signal in self.carParkMap.signals:
                if signal.marker == nextPoint.marker and signal.isCheck is False:
                    signal.isCheck = True
                    break
            time = time + self.getTravelTime(currentPoint, nextPoint)
            self.benefit = self.benefit + self.getBenefits(nextPoint, time)
            self.updateNode(nextPoint, time)
            currentPoint = copy.deepcopy(nextPoint)

    def updateSearchSpace(self, time):
        self.m_searchSpace.clear()
        for signal in self.carParkMap.signals:
            if signal.vioTime < time < signal.depTime and signal.isCheck is False:
                self.m_searchSpace.append(signal.marker)

    def walkToNextNode(self, curPT, nextPT, curTime):
        currentTime = curTime
        PT = Point()
        startP = copy.deepcopy(curPT)
        S_best = []
        best_time = -1
        self.carParkMap.initialPheromoneValues()

        for i in range(Officer.nAnts):
            self.carParkMap.ants[i].initialReDo(self.m_searchSpace)

        for i in range(Officer.maxIterCount):
            for j in range(Officer.nAnts):
                MyLog.infoTest('---------ant' + str(j))
                while currentTime < self.endTime and len(self.carParkMap.ants[j].m_path) < len(self.m_searchSpace) + 1:
                    MyLog.infoTest('Sub Path time ' + str(currentTime))
                    self.calProbability(currentTime, startP)
                    self.carParkMap.ants[j].moveToNextPoint(startP, PT, self.carParkMap.nodes, self.carParkMap.edges)

                    if startP.marker == PT.marker or PT.marker == '':
                        self.carParkMap.ants[j].antUsedTime = currentTime
                        break
                    self.carParkMap.ants[j].m_path.append(PT.marker)
                    MyLog.infoTest('m_path ' + str(self.carParkMap.ants[j].m_path))
                    currentTime = currentTime + self.getTravelTime(startP, PT)
                    startP = copy.deepcopy(PT)
                currentTime = curTime
                startP = copy.deepcopy(curPT)
            S_best = self.carParkMap.getBestACOPath(S_best, best_time)

            if len(S_best) > (len(self.m_searchSpace) - 1) or len(S_best) == 0:
                break
            self.carParkMap.updatePheromone()

            for k in range(Officer.nAnts):
                self.carParkMap.ants[k].initialReDo(self.m_searchSpace)

        if len(S_best) == 0:
            nextPT.marker = 'NULL'
        else:
            nextPT.marker = S_best[0]
